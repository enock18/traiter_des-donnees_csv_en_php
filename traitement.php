<?php
    require 'connection.php';

    $time_start = microtime(true); 
    const CSV_FILE_NEW_PATH = 'csv/people-clean.csv';
    function db_empty_table($conn, $table_name) {
        $req = 'DELETE FROM ' . $table_name;
        return $conn->query($req);
    }
    
    db_empty_table($conn, 'people');
    function verifier_email($email){
           return filter_var($email, FILTER_VALIDATE_EMAIL);            
    }
    function format_date($date, $format = 'd/m/Y', $new_format = 'Y-m-d') {
        $date = DateTime::createFromFormat($format, $date);
        return $date->format($new_format);
    }
?>

<?php       
    $ressource = fopen('csv/people.csv', 'r');
    $new_csv = fopen(CSV_FILE_NEW_PATH, 'w');
    while (($data = fgetcsv($ressource)) !== FALSE) {
        $id=$data[0];
        if(!is_numeric($id)){
            fputcsv($new_csv, $data);
            continue;
        }
        $data[3] = strtolower($data[3]);
	    $data[3] = filter_var($data[3], FILTER_VALIDATE_EMAIL) ? $data[3] : '';
        $data[4] = str_replace('politicien', 'menteur', $data[4]);
        $data[5] = format_date($data[5]);
        $data[6] = Locale::getDisplayRegion("-" . $data[6] , 'fr');
        $phonenumber= $data[7];
        $phonenumber= "0". substr($phonenumber,-9);
        $countryname=$data[6];
      
        fputcsv($new_csv,$data);
        $req = "INSERT INTO people VALUES(?,?,?,?,?,?,?,?)";
	    $res = $conn->prepare($req);
       
        if ($res->execute($data)) {
            $count++;
            var_dump($count);
        }
    }
    // echo "$count personnes correctement importés en base de données\n";
    fclose($ressource);
    fclose($new_csv);
    $time_end = microtime(true);

    // Je calcule la différence (en seconde)
    $execution_time = ($time_end - $time_start);

    // J'affiche le résultat
    echo "Temps d'execution: ".$execution_time."sec";
    
//echo $mail;
        // foreach($data as $d)
        // $length=count($data);
        //$req="INSERT INTO people (id,email) VALUES (?,?)";
        //$req="DELETE  FROM  people  where id= ?";

        //$res=$conn->prepare($req);
        //$res->execute([$id,$mail]);